(ns weecfg.core
  (:require [accountant.core :as accountant]
            [ajax.core :refer [GET]]
            [clojure.edn :refer [read-string]]
            [clojure.spec.alpha :as s]
            [reagent.core :as reagent :refer [atom]]
            [reagent.session :as session]
            [reitit.frontend :as reitit]))

; variables
(defonce form-errors (atom nil))
(defonce form-title (atom "GASP Sensor setup"))
(defonce form-data (atom {:server "" :device-id "" :wifi-ssid "" :wifi-key ""}))
(defonce device-id-regex #"[A-Za-z0-9]{4}")
(defonce alpha-numeric-regex #"[A-Za-z0-9]")

; spec validation definitions
(s/def ::boolean-type boolean?)
(s/def ::integer-type int?)
(s/def ::alpha-numeric-type (s/and string? #(re-matches alpha-numeric-regex %)))
(s/def ::ssid-size-limit #(<= (count %) 32))
(s/def ::default-size-limit #(<= (count %) 50))
(s/def ::wifi-ssid-type (s/and string?  ::ssid-size-limit))
(s/def ::device-id-type (s/and string? #(re-matches device-id-regex %)))
(s/def ::wifi-key-type (s/and string? ::default-size-limit))
(s/def ::server-type (s/and string? ::default-size-limit))


; page routes
(def router
  (reitit/router
   [["/" :index]
    ["/success" :success]
    ["/about" :about]
    ["/404" :error]]))

; link builder give a keyword from the router above return a url
(defn path-for [route & [params]]
  (if params
    (:path (reitit/match-by-name router route params))
    (:path (reitit/match-by-name router route))))

; Custom validation error messages, instead of standard cryptic spec formats.
(defn validation-message [errors default]
  (let [pred  (:cljs.spec.alpha/spec errors)]
    (case pred
      :cljs.core/string? "Sorry value must be a string!"
      :weecfg.core/size-test "Sorry value to long!"
      :weecfg.core/wifi-ssid-type "Sorry ssid invalid to long!"
      :weecfg.core/wifi-key-type "Invalid wifi key"
      :weecfg.core/device-id-type "Device is to short or invalid format."
      (or (str default pred) (str "Error not handled " (str pred))))))


; generate form fields dynamically from a hash map


(defn form-input-map [{:keys [name title placeholder validator error focus type]} data]
  [:div.mt3.dim {:key name}
   [:label.db.fw4.lh-copy.f6 {:for name} title]
   [:input#wifi-ssid.pa2.input-reset.ba.bg-near-white.w-100.measure.b--silver
    {:type (or type "text") :name name
     :placeholder placeholder :autoFocus (or focus nil)
     :on-change #(swap! data assoc (keyword name) (-> % .-target .-value))
     :value (get @data (keyword name))}]
   [:p (if (and (not-empty ((keyword name) @data))
                (s/explain-data
                 (keyword 'weecfg.core validator)
                 ((keyword name) @data)))
         (validation-message  (s/explain-data
                               (keyword 'weecfg.core validator)
                               ((keyword name) @data)) error))]])


; handle failures
(defn handle-failure [{:keys [status status-text response]}]
  (case status
    500 (do (reset! form-errors "Something bad happened submitting your data."))
    401 (do (reset! form-errors "Not authorized."))
    404 (do (reset! form-errors "Could not find device to submit data."))
    (reset! form-errors (str (:msg response) (:msg response)))))

; load in an edn map to build the form
(defn load-form [form-fields]
  (GET "/form.edn"
    {:response :transit
     :format :transit
     :error-handler handle-failure
     :handler (fn [response]
                (let [form (read-string response)
                      title (:title form)
                      fields (:fields form)]
                  (reset! form-title title)
                  (reset! form-fields fields)))}))

;submit the data as raw post data
(defn form-submit [data]
  (GET "/save"
    {:params @data
     :error-handler handle-failure
     :format :raw
     :handler (fn [response]
                (swap! form-errors "success"))}))


;simple input form, dynamically generated from edn file
(defn config-form []
  (let [form-fields (atom [])]
    (load-form form-fields)
    (fn []
      [:article.black-80.db.tc.black.ba.w-100.bg-light-green.br2
       [:form.ma3 {:method "get" :accept-charset "utf-8"
                   :on-submit    (fn [e] (.preventDefault e) (form-submit form-data))}
        (into  [:fieldset#sign_up.ba.b--transparent.ph0.mh0
                [:legend.ph0.pb3.mh0.fw6 "Device Setup"]]
               (map #(form-input-map % form-data) @form-fields))
        (if @form-errors
          [:div.flex.items-center.justify-center.pa3.bg-lightest-blue.navy
           [:i.fas.fa-exclamation-circle]
           [:span.lh-title.ml3 @form-errors]])
        [:div.mt3
         [:input.b.ph3.pv2.input-reset.ba.b--black.bg-transparent.grow.pointer.f6.br2
          {:type "submit" :value "Save"}]]]])))

; homepage is a simple form to enter your data
(defn home-page []
  [:div
   [config-form]])

(defn success-page []
  [:div
   [:div [:h1.m-4 "Settings saved"]
    [:p "Device will reboot in a few seconds and connect to your wifi."]]])

(defn about-page []
  [:div.items-center.jutify-center
   [:div [:h1.m-4 "About Page"]
    [:p "Device configuration page built using "
     [:a {:href "https://gitlab.com/olymk2/weecfg" :title "weecfg"} "weecfg"]
     " set up your device wifi connection and other device specific settings."]
    [:a.link {:href (path-for :index)} "Back"]]])

; given a browser route load the relevant defn handler
(defn page-for [route]
  "Map your route keys to pages here"
  (print route)
  (case route
    :index (var home-page)
    ;:error #'404-page
    :success (success-page)
    :about (var about-page)
    (var home-page)))

; default page html wraps the current page elements
(defn current-page []
  (fn []
    (let [page (:current-page (session/get :route))]
      [:div.bg-green.flex.justify-center.items-center.h-100.w-100
       [:div..w-75.w-50-m
        [:h2.w-100
         [:a.link.fr.black.pt3.pb3 {:href (path-for :about)} [:i.fas.fa-question-circle]]
         [:a.athelas.f3.link.white-70.hover-white.no-underline.flex.items-center.pa3
          {:href "/"}
          [:i.fas.fa-surprise.mr3.v-mid] @form-title]]
        [page]]])))

; sort out routing sessions and basically kickstart everything
(defn startup []
  (accountant/configure-navigation!
   {:nav-handler
    (fn [path]
      (let [match (reitit/match-by-path router path)
            current-page (:name (:data  match))
            route-params (:path-params match)]
        (session/put! :route {:current-page (page-for current-page)
                              :route-params route-params})))
    :path-exists?
    (fn [path]
      (boolean (reitit/match-by-path router path)))})
  (accountant/dispatch-current!)
  (reagent/render [current-page]
                  (.getElementById js/document "app")))

(startup)
