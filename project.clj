(defproject weecfg "0.1.0-SNAPSHOT"
  :uberjar-name "server.jar"
  :target-path "target/%s"
  :source-paths ["src" "src/clj" "src/cljs"]
  :ring {:handler server-rendering.handler/app}
  :plugins [[lein-tools-deps "0.4.5"] [lein-ring "0.12.5"]]
  :middleware [lein-tools-deps.plugin/resolve-dependencies-with-deps-edn]
  :lein-tools-deps/config {:config-files [:install :user :project]})
